<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

use Restserver\Libraries\REST_Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Authentication extends REST_Controller{
    public function __construct(){
        parent::__construct();

        // Load model
        $this->load->model('Authentication_model', 'auth');

        // Load helper to create token
        $this->load->helper(['jwt', 'authorization']);
    }

    // Token Generate Test
    public function hello_get()
    {
        $tokenData = 'Hello World!';
        
        // Create a token
        $token = AUTHORIZATION::generateToken($tokenData);
        // Set HTTP status code
        $status = parent::HTTP_OK;
        // Prepare the response
        $response = ['status' => $status, 'token' => $token];
        // REST_Controller provide this method to send responses
        $this->response($response, $status);
    }

    public function login_post(){
        // Have dummy user details to check user credentials
        // send via postman
        $data = [
            'username' => $this->post('username'),
            'password' => md5($this->post('password'))
        ];

        // Extract user data from POST request
        $cek = $this->auth->cekdata($data);

        // Check if valid user
        if ($cek->num_rows() > 0) {
            
            // Create a token from the user data and send it as reponse
            $token = AUTHORIZATION::generateToken(['nama_user' => $data['username']]);

            // Prepare the response
            $status = parent::HTTP_OK;
            $response = [
                'status' => $status, 
                'token' => $token
            ];
            $this->response($response, $status);
        }
    }

    private function verify_request(){
        // Get all the headers
        $headers = $this->input->request_headers();

        // Extract the token
        $token = $headers['Authorization'];

        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }
    }

    public function get_me_data_post(){
        // Call the verification method and store the return value in the variable
        $data = $this->verify_request();
        
        // Send the return data as reponse
        $status = parent::HTTP_OK;
        $response = ['status' => $status, 'data' => $data];
        $this->response($response, $status);
    }
}