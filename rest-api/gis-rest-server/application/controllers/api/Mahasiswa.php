<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

use Restserver\Libraries\REST_Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Mahasiswa extends REST_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Mahasiswa_model', 'mahasiswa');
        $this->load->helper(['jwt', 'authorization']);
        //Mengubah pengaturan Keys dan Limit pada rest.php
        //authentikasi login pengturannya ada pada rest.php
        //Nama keys adalah gis-key dan keynya adalah gis123
        //authorizationnya basic auth username edsel password gis1234
        //Melimit request method get menjadi 2 kali per jam
        //$this->methods['index_get']['limit'] = 2;
    }

    private function verify_request(){
        // Get all the headers
        $headers = $this->input->request_headers();

        // Extract the token
        $token = $headers['Authorization'];

        // Use try-catch
        // JWT library throws exception if the token is not valid
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data === false) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
                exit();
            } else {
                return $data;
            }
        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }
    }

    public function index_get(){
        $cektoken = $this->verify_request();
        $id = $this->get('id');
        if($id == null){
            $mahasiswa = $this->mahasiswa->getMahasiswa();
        }else{
            $mahasiswa = $this->mahasiswa->getMahasiswa($id);
        }

        if($mahasiswa){
            $this->response([
                'status' => TRUE,
                'data' => $mahasiswa
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Id Tidak Ditemukan'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_delete(){
        $cektoken = $this->verify_request();
        $id = $this->delete('id');

        if($id == null){
            $this->response([
                'status' => FALSE,
                'message' => 'Tolong Masukkan Id Untuk Melakukan Delete Data'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }else{
            if($this->mahasiswa->deleteMahasiswa($id) > 0){
                $this->response([
                    'status' => TRUE,
                    'id' => $id,
                    'message' => 'Data Berhasil Dihapus'
                ], REST_Controller::HTTP_NO_CONTENT);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Delete Data Gagal Id Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function index_post(){
        $cektoken = $this->verify_request();
        $data = [
            'id_wilayah' => $this->post('id_wilayah'),
            'jml_if' => $this->post('jml_if'),
            'jml_gc' => $this->post('jml_gc'),
            'radius' => $this->post('radius'),
            'warna' => $this->post('warna')
        ];

        if($this->mahasiswa->createMahasiswa($data) > 0){
            $this->response([
                'status' => TRUE,
                'message' => 'Data Baru Berhasil Dibuat'
            ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Baru Gagal Dibuat'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put(){
        $cektoken = $this->verify_request();
        $id = $this->put('id');
        $data = [
            'id_wilayah' => $this->put('id_wilayah'),
            'jml_if' => $this->put('jml_if'),
            'jml_gc' => $this->put('jml_gc'),
            'radius' => $this->put('radius'),
            'warna' => $this->put('warna')
        ];

        if($this->mahasiswa->updateMahasiswa($data, $id) > 0){
            $this->response([
                'status' => TRUE,
                'message' => 'Data Berhasil Diubah'
            ], REST_Controller::HTTP_OK);
        }
    }
}

?>