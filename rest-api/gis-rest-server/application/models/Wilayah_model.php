<?php 

class Wilayah_model extends CI_model{
    public function getWilayah($id = null){
        if($id == null){
            return $this->db->get('tbl_wilayah')->result_array();
        }else{
            return $this->db->get_where('tbl_wilayah', ['wilayah_id' => $id])->result_array();
        }
    }

    public function deleteWilayah($id){
        $this->db->delete('tbl_wilayah', ['wilayah_id' => $id]);
        return $this->db->affected_rows();
    }

    public function createWilayah($data){
        $this->db->insert('tbl_wilayah', $data);
        return $this->db->affected_rows();
    }

    public function updateWilayah($data, $id){
        $this->db->update('tbl_wilayah', $data, ['wilayah_id' => $id]);
        return $this->db->affected_rows();
    }

    public function getNamaWilayah($wilayah_id = null){
        $this->db->from('tbl_wilayah');
        if($wilayah_id != null){
            $this->db->where('wilayah_id', $wilayah_id)->result_array();
        }
        $query = $this->db->get()->result_array();
        return $query;
    }
}

?>