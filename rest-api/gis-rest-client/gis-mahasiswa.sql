-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2020 at 10:49 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gis-mahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, 'gis123', 1, 0, 0, NULL, 1),
(2, 3, 'rahasia', 1, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mahasiswa`
--

CREATE TABLE `tbl_mahasiswa` (
  `id` int(11) NOT NULL,
  `id_wilayah` int(11) NOT NULL,
  `jml_if` int(11) DEFAULT NULL,
  `jml_gc` int(11) DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `warna` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`id`, `id_wilayah`, `jml_if`, `jml_gc`, `radius`, `warna`, `created`, `updated`) VALUES
(19, 35, 1, 0, 500, 'red', '2020-06-18 02:48:49', '2020-06-18 02:49:27'),
(20, 37, 3, 1, 500, 'red', '2020-06-18 03:36:16', NULL),
(21, 38, 2, 0, 500, 'red', '2020-06-18 03:36:44', NULL),
(22, 39, 1, 0, 500, 'red', '2020-06-18 03:36:59', NULL),
(23, 40, 1, 0, 500, 'blue', '2020-06-18 03:37:25', NULL),
(24, 36, 4, 0, 500, 'green', '2020-06-18 03:37:43', '2020-06-18 03:43:10'),
(25, 41, 1, 0, 500, 'blue', '2020-06-18 03:38:11', NULL),
(26, 42, 1, 0, 500, 'red', '2020-06-18 03:38:24', NULL),
(27, 43, 1, 0, 500, 'green', '2020-06-18 03:38:51', '2020-06-18 03:43:26'),
(28, 44, 1, 0, 500, 'red', '2020-06-18 03:39:10', NULL),
(29, 45, 1, 0, 500, 'red', '2020-06-18 03:40:03', NULL),
(30, 46, 0, 1, 500, 'red', '2020-06-18 03:40:28', NULL),
(31, 19, 0, 2, 500, 'red', '2020-06-18 03:40:42', NULL),
(32, 47, 0, 1, 500, 'green', '2020-06-18 03:41:03', '2020-06-18 03:43:57'),
(33, 48, 1, 0, 500, 'red', '2020-06-18 03:41:57', NULL),
(34, 49, 1, 0, 500, 'green', '2020-06-18 03:42:11', '2020-06-18 03:44:11'),
(35, 50, 1, 0, 500, 'green', '2020-06-18 03:42:29', '2020-06-18 03:44:19'),
(36, 51, 1, 0, 500, 'red', '2020-06-18 03:42:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `nama_user`, `username`, `password`) VALUES
(1, 'Edsel Jayadi', 'EdselGIS', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wilayah`
--

CREATE TABLE `tbl_wilayah` (
  `wilayah_id` int(11) NOT NULL,
  `nama_wilayah` varchar(255) NOT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wilayah`
--

INSERT INTO `tbl_wilayah` (`wilayah_id`, `nama_wilayah`, `provinsi`, `kabupaten`, `kecamatan`, `latitude`, `longitude`) VALUES
(19, 'Pondok Kelapa', 'DKI Jakarta', 'Jakarta Timur', 'Duren Sawit', '-6.242353510448045', '106.93310757057361'),
(35, 'Kelapa Gading Timur', 'DKI Jakarta', 'Jakarta Utara', 'Kelapa Gading', '-6.168374318738085', '106.90418055415732'),
(36, 'Pejuang', 'Jawa Barat', 'Bekasi', 'Medan Satria', '-6.180150247474219', '106.9890668457607'),
(37, 'Bali Mester', 'DKI Jakarta', 'Jakarta Timur', 'Jatinegara', '-6.22059600363985', '106.86658859252931'),
(38, 'Rawa Badak Utara', 'DKI Jakarta', 'Jakarta Utara', 'Koja', '-6.11981718850008', '106.89783044453486'),
(39, 'Rawa Badak Selatan', 'DKI Jakarta', 'Jakarta Utara', 'Koja', '-6.131167494111519', '106.89954718795437'),
(40, 'Ciriung', 'Jawa Barat', 'Bogor', 'Cibinong', '-6.462181073316725', '106.86581579220807'),
(41, 'Gunung Putri', 'Jawa Barat', 'Bogor', 'Bojong Kulur', '-6.464739627382506', '106.89070695116624'),
(42, 'Pulo Gadung', 'DKI Jakarta', 'Jakarta Timur', 'Pulo Gadung', '-6.18262486585818', '106.90160757825274'),
(43, 'Bojong Menteng', 'Jawa Barat', 'Bekasi', 'Rawa Lumbu', '-6.300966287777954', '106.98692321777345'),
(44, 'Klender', 'DKI Jakarta', 'Jakarta Timur', 'Duren Sawit', '-6.21846286621148', '106.90658569335938'),
(45, 'Sumur Batu', 'DKI Jakarta', 'Jakarta Pusat', 'Kemayoran', '-6.163680940877508', '106.87010778040681'),
(46, 'Mangga Besar', 'DKI Jakarta', 'Jakarta Barat', 'Taman Sari', '-6.145589714400061', '106.81860917284591'),
(47, 'Bintara Jaya', 'Jawa Barat', 'Bekasi', 'Bekasi Barat', '-6.248070038918241', '106.95447993060418'),
(48, 'Kebon Manggis', 'DKI Jakarta', 'Jakarta Timur', 'Matraman', '-6.20754106706639', '106.85680389404298'),
(49, 'Bekasi Jaya', 'Jawa Barat', 'Bekasi', 'Bekasi Timur', '-6.241414969959435', '107.03782075121507'),
(50, 'Jatiwaringin', 'Jawa Barat', 'Bekasi', 'Pondok Gede', '-6.272556550560602', '106.91473967358965'),
(51, 'Warakas', 'DKI Jakarta', 'Jakarta Utara', 'Tanjung Priok', '-6.121566687423038', '106.87752976884791');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_wilayah` (`id_wilayah`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_wilayah`
--
ALTER TABLE `tbl_wilayah`
  ADD PRIMARY KEY (`wilayah_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_wilayah`
--
ALTER TABLE `tbl_wilayah`
  MODIFY `wilayah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD CONSTRAINT `tbl_mahasiswa_ibfk_1` FOREIGN KEY (`id_wilayah`) REFERENCES `tbl_wilayah` (`wilayah_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
