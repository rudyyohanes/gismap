<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Home extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){
            $url = base_url('Login');
            redirect($url);
        }
        $this->load->model('M_home');
    }

    public function index()
    {
        $data = array(
            'title' => 'Pemetaan Tempat Tinggal Mahasiswa',
            'pemetaan' => $this->M_home->show_data(),
            'content' => 'v_home',
        );
        $this->load->view('layout/v_wrapper', $data, FALSE);
    }

    public function input(){
        $this->form_validation->set_rules('id_wilayah', 'Nama Wilayah', 'callback_fk_wilayah_check');
        $this->form_validation->set_rules('jml_if', 'Informatika', 'required');
        $this->form_validation->set_rules('jml_gc', 'Game Computing', 'required');
        $this->form_validation->set_rules('radius', 'Radius', 'required');
        $this->form_validation->set_rules('warna', 'Warna', 'required');

        $wilayah = $this->M_home->get_wilayah();

        if($this->form_validation->run() == FALSE){
            $data = array(
                'title' => 'Input Data Circle Mahasiswa',
                'content' => 'v_input',
                'wilayah' => $wilayah
            );
            $this->load->view('layout/v_wrapper', $data, FALSE);
        }else{
            $data = array(
                'id_wilayah' => $this->input->post('id_wilayah'),
                'jml_if' => $this->input->post('jml_if'),
                'jml_gc' => $this->input->post('jml_gc'),
                'radius' => $this->input->post('radius'),
                'warna' => $this->input->post('warna')
            );
            $this->M_home->input($data);
            $this->session->set_flashdata('pesan', 'Data Berhasil Disimpan!');
            redirect('home/input');
        }
    }

    public function input_wilayah(){
        $this->form_validation->set_rules('nama_wilayah', 'Nama Wilayah', 'callback_wilayah_check');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');

        if($this->form_validation->run() == FALSE){
            $data = array(
                'title' => 'Input Data Wilayah',
                'content' => 'v_input_wilayah'
            );
            $this->load->view('layout/v_wrapper', $data, FALSE);
        }else{
            $data = array(
                'nama_wilayah' => $this->input->post('nama_wilayah'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'provinsi' => $this->input->post('provinsi'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan')
            );
            $this->M_home->input_wilayah($data);
            $this->session->set_flashdata('pesan', 'Data Berhasil Disimpan!');
            redirect('home/input_wilayah');
        }
    }

    public function data_mahasiswa(){
        $data = array(
            'title' => 'Data Tempat Tinggal Mahasiswa',
            'mahasiswa' => $this->M_home->show_data(),
            'content' => 'v_data_mahasiswa'
        );
        $this->load->view('layout/v_wrapper', $data, FALSE);
    }

    public function data_wilayah(){
        $data = array(
            'title' => 'Data Wilayah',
            'wilayah' => $this->M_home->show_data_wilayah(),
            'content' => 'v_data_wilayah'
        );
        $this->load->view('layout/v_wrapper', $data, FALSE);
    }

    public function edit($id){
        $this->form_validation->set_rules('id_wilayah', 'Nama Wilayah', 'required');
        $this->form_validation->set_rules('jml_if', 'Informatika', 'required');
        $this->form_validation->set_rules('jml_gc', 'Game Computing', 'required');
        $this->form_validation->set_rules('radius', 'Radius', 'required');
        $this->form_validation->set_rules('warna', 'Warna', 'required');

        $wilayah = $this->M_home->get_wilayah();

        if($this->form_validation->run() == FALSE){
            $data = array(
                'title' => 'Edit Data Mahasiswa',
                'content' => 'v_edit',
                'data' => $this->M_home->detail($id),
                'wilayah' => $wilayah
            );
            $this->load->view('layout/v_wrapper', $data, FALSE);
        }else{
            $data = array(
                'id' => $id,
                'id_wilayah' => $this->input->post('id_wilayah'),
                'jml_if' => $this->input->post('jml_if'),
                'jml_gc' => $this->input->post('jml_gc'),
                'radius' => $this->input->post('radius'),
                'warna' => $this->input->post('warna')
            );
            $this->M_home->edit($data);
            $this->session->set_flashdata('pesan', 'Data Berhasil Disimpan!');
            redirect('home/data_mahasiswa');
        }
    }

    public function edit_wilayah($wilayah_id){
        $this->form_validation->set_rules('nama_wilayah', 'Nama Wilayah', 'required');        
        $this->form_validation->set_rules('latitude', 'Latitude', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');

        if($this->form_validation->run() == FALSE){
            $data = array(
                'title' => 'Edit Data Wilayah',
                'pemetaan' => $this->M_home->show_data(),
                'data' => $this->M_home->detail_wilayah($wilayah_id),
                'content' => 'v_edit_wilayah'
            );
            $this->load->view('layout/v_wrapper', $data, FALSE);
            $this->session->set_flashdata('gagal_edit', 'Data Gagal Diubah!');
        }else{
            $data = array(
                'wilayah_id' => $wilayah_id,
                'nama_wilayah' => $this->input->post('nama_wilayah'),                
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'provinsi' => $this->input->post('provinsi'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan')
            );
            $this->M_home->edit_wilayah($data);
            $this->session->set_flashdata('pesan', 'Data Berhasil Diubah!');
            redirect('home/data_wilayah');
        }
    }

    public function delete($id){
        $this->M_home->delete($id);
        $this->session->set_flashdata('pesan', 'Data Berhasil Dihapus!');
        redirect('home/data_mahasiswa');
    }

    public function delete_wilayah($wilayah_id){
        $delmd = $this->M_home->delete_wilayah($wilayah_id);
        if ($this->session->userdata('deleted') == TRUE) {
            $this->session->set_flashdata('pesan', 'Data Berhasil Dihapus!');
        }else{
            $this->session->set_flashdata('gagal_delete', 'Data Gagal Dihapus Karena Data Sedang Digunakan di Data Mahasiswa!');
        }
        redirect('home/data_wilayah');
    }

    public function wilayah_check($nama_wilayah){
        $this->session->set_flashdata('gagal_input_wilayah', 'Data Untuk WIlayah Ini Sudah Ada');
        if($this->M_home->checkWilayah($nama_wilayah)){
            return true;
        } else {
            return false;
        }
    }

    public function fk_wilayah_check($id){
        $checkWilayah = $this->M_home->checkFkWilayah($id);
        if ($checkWilayah->num_rows() > 0)
        {
            $this->session->set_flashdata('gagal_input_mhs', 'Data Untuk WIlayah Ini Sudah Ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function Logout(){
        $this->session->sess_destroy();
        $url = base_url('Dashboard');
        redirect($url);
    }
}