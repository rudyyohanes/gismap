<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index()
	{
		$this->load->view('V_login');
	}

    public function authentication()
	{
		$id = strip_tags(str_replace("'", "", $this->input->post('username')));
		$password = strip_tags(str_replace("'", "", $this->input->post('password')));

		$data['nonxssData'] = array
		(
			'username' => $id,
			'password' => $password,
			'gis-key' => 'gis123'
		);

		$data['xssData'] = $this->security->xss_clean($data['nonxssData']);

		$cadmin = $this->M_login->cekdata($data['xssData']);

		if($cadmin == TRUE)
		{
			$this->session->set_userdata('masuk', true);
			$this->session->set_userdata('token', $cadmin['token']);
			// $this->session->set_userdata('user', $u);

			// $xcadmin = $cadmin->row_array();
			$idadmin = $xcadmin['username'];
			$user_nama = $xcadmin['nama_user'];

			$this->session->set_userdata('idadmin', $idadmin);
			$this->session->set_userdata('name', $user_nama);
		}
        if($this->session->userdata('masuk') == true){
            redirect('Home');
        }else{
            $url = base_url('Login');
            echo $this->session->set_flashdata('msg', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Username atau Password salah</div>');
            redirect($url);
        }
	}
}
?>