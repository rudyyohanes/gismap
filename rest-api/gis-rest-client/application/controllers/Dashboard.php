<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class Dashboard extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('M_dashboard');
    }

    public function index()
    {
        $data = array(
            'mahasiswa' => $this->M_dashboard->show_data(),
            'jml_if' => $this->M_dashboard->sum_if(),
            'jml_gc' => $this->M_dashboard->sum_gc(),
            'jml_wil' => $this->M_dashboard->jml_wil()
        );
        $this->load->helper('date');
        date_default_timezone_set("Asia/Jakarta");
        $this->load->view('layout/tes2', $data, FALSE);
    }
}
?>