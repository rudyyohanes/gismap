<?php

defined('BASEPATH') OR exit('No direct script acces allowed');

class M_dashboard extends CI_Model{
    public function show_data(){
        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->join('tbl_wilayah', 'tbl_wilayah.wilayah_id = tbl_mahasiswa.id_wilayah');
        $this->db->order_by('id', 'desc');
        return $this->db->get()->result();
    }

    public function sum_if(){
        $this->db->select_sum('jml_if');
        $query = $this->db->get('tbl_mahasiswa');
        return $query->result();
    }

    public function sum_gc(){
        $this->db->select_sum('jml_gc');
        $query = $this->db->get('tbl_mahasiswa');
        return $query->result();
    }

    public function jml_wil(){
        return $this->db->count_all_results('tbl_wilayah');
    }

    public function last_updated(){
        $this->db->select('updated');
        $query = $this->db->get('tbl_mahasiswa');
        return $query->result();
    }
}