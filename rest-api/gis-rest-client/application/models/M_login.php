<?php
use GuzzleHttp\Client;

defined('BASEPATH') OR exit('No direct script acces allowed');

class M_login extends CI_Model{
	private $_client;

    public function __construct(){
        $this->_client = new Client([
            'base_uri' => 'http://localhost/rest-api/gis-rest-server/api/'
        ]);
	}
	
	function cekdata($data){
		$response = $this->_client->request('POST', 'authentication/login', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result;
	}
}