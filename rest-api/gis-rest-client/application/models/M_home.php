<?php
use GuzzleHttp\Client;

defined('BASEPATH') OR exit('No direct script acces allowed');

class M_home extends CI_Model{

    private $_client;

    public function __construct(){
        $headers = [
            'Authorization' => $this->session->userdata('token'),
            'Accept'        => 'application/json'
        ];
        $this->_client = new Client([
            'base_uri' => 'http://localhost/rest-api/gis-rest-server/api/',
            'headers' => $headers
        ]);
    }

    //input
    public function input($data){
        $response = $this->_client->request('POST', 'mahasiswa', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result;
    }

    public function input_wilayah($data){
        $response = $this->_client->request('POST', 'wilayah', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result;
    }

    //show data
    public function show_data(){
        try {
            $response = $this->_client->request('GET', 'mahasiswa');

            $result = json_decode($response->getBody()->getContents(), TRUE);
            return $result['data'];
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = $e->getResponse();
            $responseBodyAsString = json_decode($res->getBody()->getContents(), FALSE);
            return $responseBodyAsString;
        }
    }

    public function show_data_wilayah(){
        $response = $this->_client->request('GET', 'wilayah');

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result['data'];
    }

    //ambil detail
    public function detail($id){
        $response = $this->_client->request('GET', 'mahasiswa', [
            'query' => [
                'id' => $id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result['data'][0];
    }

    public function detail_wilayah($wilayah_id){
        $response = $this->_client->request('GET', 'wilayah', [
            'query' => [
                'wilayah_id' => $wilayah_id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result['data'][0];
    }

    //checking
    function checkWilayah($nama_wilayah){
        $query = $this->db->get_where('tbl_wilayah', array('nama_wilayah' => $nama_wilayah));
        if(empty($query->row_array())){
            return true;
        } else {
            return false;
        }
    }

    function checkFKWilayah($id){
        $sql = "SELECT id_wilayah FROM tbl_mahasiswa WHERE id_wilayah = ? ";
        $hasil = $this->db->query($sql,array($id));
        return $hasil;
    }

    //edit
    public function edit($data){
        try {
            $response = $this->_client->request('PUT', 'mahasiswa', [
                'form_params' => $data
            ]);
    
            $result = json_decode($response->getBody()->getContents(), TRUE);
            return $result;
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            $res = $e->getResponse();
            $responseBodyAsString = json_decode($res->getBody()->getContents(), FALSE);
            return $responseBodyAsString;
        }
    }

    public function edit_wilayah($data){
        $response = $this->_client->request('PUT', 'wilayah', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result;
    }

    //delete
    public function delete($id){
        $response = $this->_client->request('DELETE', 'mahasiswa', [
            'form_params' => [
                'id' => $id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result;
    }

    public function delete_wilayah($wilayah_id){
        try {
            $response = $this->_client->request('DELETE', 'wilayah', [
                'form_params' => [
                    'wilayah_id' => $wilayah_id
                ]
            ]);
    
            $result = json_decode($response->getBody()->getContents(), TRUE);
            $this->session->set_userdata('deleted', TRUE);
            return $result;
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            $res = $e->getResponse();
            $responseBodyAsString = json_decode($res->getBody()->getContents(), FALSE);
            $this->session->set_userdata('deleted', FALSE);
            return $responseBodyAsString;
        }
    }

    //get
    // public function get($id = null){
    //     $this->db->from('tbl_mahasiswa');
    //     if($id != null){
    //         $this->db->where('id', $id);
    //     }
    //     $query = $this->db->get();
    //     return $query;
    // }

    public function get_wilayah($wilayah_id = null){
        $response = $this->_client->request('GET', 'wilayah/namaWilayah_get', [
            'query' => [
                'wilayah_id' => $wilayah_id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), TRUE);
        return $result['data'];
    }
}