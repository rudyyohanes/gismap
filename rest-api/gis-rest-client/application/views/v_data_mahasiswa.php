<?php
if($this->session->flashdata('pesan')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('pesan');
    echo '</div>';
}else if($this->session->flashdata('gagal_edit_mhs')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('gagal_edit_mhs');
    echo '</div>';
}

echo form_open('home/input');
?>

<table class="table table-responsive table-striped table-bordered table-hover" id="table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama Wilayah</th>
            <th>Informatika</th>
            <th>Game Computing</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($mahasiswa as $key => $value) { ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $value['nama_wilayah'] ?></td>
                <td><?= $value['jml_if'] ?></td>
                <td><?= $value['jml_gc'] ?></td>
                <td>
                    <a href="<?= base_url('home/edit/'.$value['id']) ?>" class="btn btn-xs btn-success">Edit</a>
                    <a href="<?= base_url('home/delete/'.$value['id']) ?>" class="btn btn-xs btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>