<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="<?= base_url() ?>template/assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a  href="<?= base_url('Home') ?>"><i class="fa fa-map-marker fa-3x"></i> Pemetaan</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-table fa-3x"></i> Table Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  href="<?= base_url('home/data_mahasiswa') ?>"> Data Mahasiswa</a>
                            </li>
                            <li>
                                <a  href="<?= base_url('home/data_wilayah') ?>"> Data Wilayah</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-plus fa-3x"></i> Input Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">                           
                            <li>
                                <a  href="<?= base_url('home/input') ?>"> Input Data Circle Mahasiswa</a>
                            </li>
                            <li>
                                <a  href="<?= base_url('home/input_wilayah') ?>"> Input Data Wilayah</a>
                            </li>
                        </ul>
                    </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2><?= $title ?></h2>   
                        </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />