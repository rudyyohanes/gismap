<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
    <title>GIS Kalbiser</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
      <meta charset="utf-8">
    <!-- Map & Bootstrap -->
      <link href="<?= base_url() ?>template/assets/css/leaflet.css" rel="stylesheet"/>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
      <link href="<?= base_url() ?>template/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="https://unpkg.com/simplebar@latest/dist/simplebar.css"> -->
      <link href="<?= base_url() ?>template/assets/css/simplebar.css" rel="stylesheet" />
      <link href="<?= base_url() ?>template/assets/css/dashboard.css" rel="stylesheet" />
</head>

<body class="w-100">
  <div class="container-fluid">
  <div id="header-kiri"><img src="<?= base_url() ?>template/assets/img/logo.jpg" style="width:200px;"/></div>
    <header>Kalbiser Home Mapping by GIS Group 04PAIF at Kalbis Institute</header>
    <div id="header-kanan1">
        <?php $format = "%d-%m-%Y %H:%i"; echo @mdate($format);?>
    </div>
    <div id="header-kanan"><a class="btn btn-outline-light" href="<?= base_url('Login') ?>">Login</a></div>
    <aside>Total Mahasiswa <br/>
          <?php $sum1=0; foreach ($jml_if as $total){$sum1+=$total->jml_if;}?>
          <?php $sum2=0; foreach ($jml_gc as $total){$sum2+=$total->jml_gc;}?>
          <?php echo $sum1+$sum2 ?>
    </aside>
    <div id="kananatas1">Total Mahasiswa IF <br/><?php $sum=0; foreach ($jml_if as $total){$sum+=$total->jml_if;}?>
      <?php echo $sum ?></div>
    <div id="kananatas2">Total Mahasiswa GC <br/><?php $sum=0; foreach ($jml_gc as $total){$sum+=$total->jml_gc;}?>
      <?php echo $sum ?></div>
    <div id="kiri" data-simplebar class="list-group">Wilayah dan Jumlah <br/>Mahasiswa
        <div id="spasi" style = "margin-bottom:5px;"></div>
      <?php foreach ($mahasiswa as $key => $value) { ?>
        <li class="list-group-item list-group-item-action list-group-item-dark">
        <?= $value->nama_wilayah ?> : <?= $value->jml_if + $value->jml_gc?></li>
      <?php } ?>
    </div>
    <main id="map"></main>
    <div id="kanan1" data-simplebar>
      <ul class="list-group list-group-flush">
        <?php foreach ($mahasiswa as $key => $value) { ?>
        <li class="list-group-item list-group-item-action list-group-item-dark">
        <?= $value->nama_wilayah ?> : <?= $value->jml_if?></li>
        <?php } ?>
      </ul>
    </div>
    <div id="kanan2" data-simplebar class="list-group">
      <?php foreach ($mahasiswa as $key => $value) { ?>
        <li class="list-group-item list-group-item-action list-group-item-dark">
        <?= $value->nama_wilayah ?> : <?= $value->jml_gc?></li>
      <?php } ?>
    </div>
    <div id="footer-kiri">jumlah wilayah<br/>
      <?php echo $jml_wil ?>
    </div>
    <footer>Copyright © 2020 GIS Group 04PAIF Kalbis Institute. All rights reserved.</footer>
  </div>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="crossorigin=""></script>
  <script>
      var map = L.map('map').setView([-6.342526, 106.866983], 10);

      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
          maxZoom: 18,
          id: 'mapbox/streets-v11'
      }).addTo(map);

      <?php foreach ($mahasiswa as $key => $value) { ?>
          L.circle([<?= $value->latitude ?>, <?= $value->longitude ?>], {
              radius: <?= $value->radius ?>,
              color: '',
              fillColor: '<?= $value->warna ?>',
              fillOpacity: 0.5,
              }).bindPopup("<b><?= $value->nama_wilayah ?></b><br>Game Computing : <?= $value->jml_gc ?><br>Informatika : <?= $value->jml_if ?>").addTo(map);
      <?php } ?>
  </script>
  <script src="https://unpkg.com/simplebar@latest/dist/simplebar.min.js"></script>
</body>