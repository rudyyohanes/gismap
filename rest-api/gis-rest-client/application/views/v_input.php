<div class = "col-sm-5"> 
<?php
if($this->session->flashdata('pesan')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('pesan');
    echo '</div>';
}else if($this->session->flashdata('gagal_input_mhs')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('gagal_input_mhs');
    echo '</div>';
}

echo form_open('home/input');
?>

    <div class="form-group">
        <label>Pilih Wilayah</label>
        <select name="id_wilayah" class="form-control" required>
            <option value="">- Pilih -</option>
            <?php foreach ($wilayah as $key => $value) { ?>
                <option value="<?= $value['wilayah_id'] ?>"><?= $value['nama_wilayah'] ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Informatika</label>
            <input type="number" name="jml_if" placeholder="Informatika" class="form-control" required>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Game Computing</label>
            <input type="number" name="jml_gc" placeholder="Game Computing" class="form-control" required>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Radius</label>
            <select name="radius" class="form-control">
                <option value="">- Pilih -</option>
                <option value="500">001 - 100</option>
                <option value="600">101 - 200</option>
                <option value="700">201 - 300</option>
                <option value="800">301 - 400</option>
                <option value="900">401 - 500</option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Warna</label>
            <select name="warna" class="form-control">
                <option value="">- Pilih -</option>
                <option value="red">Merah</option>
                <option value="blue">Biru</option>
                <option value="yellow">Kuning</option>
                <option value="green">Hijau</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    </div>

<?php
echo form_close();
?>
</div>