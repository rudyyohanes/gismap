<?php $this->load->view('V_loginheader'); ?>
<body>
        <!--Intro Section-->
        <section class="view intro-2 animated fadeIn">
          <div class="mask rgba-stylish-strong h-100 d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-lg-5">

                        <!--Form with header-->
                        <div class="card animated fadeInDownBig">
                            <div class="card-body">

                                <!--Header-->
                                <div class="form-header aqua-gradient">
                                    <h3><i class="fa fa-user mt-2 mb-2"></i> Log in:</h3>
                                </div>
                                <?php echo $this->session->flashdata('msg'); ?>
                                <?php
                                	$this->load->helper('form');
                                	$formAttribute = array
                                	(
                                		'class' => 'form floating-label',
                                		'accept-charset' => 'utf-8' //uts-8 type => utf-8
                                	);
                                	echo form_open(base_url().'Login/authentication', $formAttribute);
                                ?>
                                <!--Body-->
                               <div class="md-form">
                                    <i class="fa fa-user prefix white-text"></i>
                                    <!--<input type="text" id="orangeForm-name" class="form-control">-->
                                    <?php
                                    	$useridAttribute = array
                                    	(
                                    		'type' => 'text',
                                    		'class' => 'form-control text-light', //tambah class text-light
                                    		'id' => 'orangeForm-name',
                                    		'name' => 'username',
                                    		'autocomplete' => 'off',
                                    		'required' => '' //placeholder dihapus
                                    	);
                                    	echo form_input($useridAttribute);
                                    ?>
                                    <label for="orangeForm-name">Username</label>
                                </div>

                                <div class="md-form">
                                    <i class="fa fa-lock prefix white-text"></i>
                                    <!--<input type="password" id="orangeForm-pass" class="form-control">-->
                                    <?php
                                    	$passwordAttribute = array
                                    	(
                                    		'type' => 'password',
                                    		'class' => 'form-control text-light', //tambah class text-light
                                    		'id' => 'orangeForm-pass',
                                    		'name' => 'password',
                                    		'autocomplete' => 'off',
                                    		'required' => '' //placeholder dihapus
                                    	);
                                    	echo form_input($passwordAttribute);
                                    ?>
                                    <label for="orangeForm-pass">Your password</label>
                                </div>

                                <div class="text-center">
                                    <!-- <button class="btn aqua-gradient btn-lg">Log In</button> -->
                                    <?php
                                    	$submitAttribute = array
                                    	(
                                    		'type' => 'submit',
                                    		'class' => 'btn aqua-gradient btn-lg',
                                    		'value' => 'Login'
                                    	);
                                    	echo form_input($submitAttribute);
                                    ?>
                                    <hr>
                                    
                                </div>
                            </div>
                        </div>
                        <!--/Form with header-->
                        <?php
                        	echo form_close();
                        ?>
                    </div>
                </div>
            </div>
          </div>
        </section>

    </header>
    <!-- Footer -->
    <?php $this->load->view('V_loginscript'); ?>
<!-- Footer -->
