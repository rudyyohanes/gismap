<div class = "col-sm-7"> 
    <div id="map" style="height:500px"></div>
</div>

<div class = "col-sm-5"> 
<?php
if($this->session->flashdata('pesan')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('pesan');
    echo '</div>';
}else if($this->session->flashdata('gagal_input_wilayah')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('gagal_input_wilayah');
    echo '</div>';
}

echo form_open('home/input_wilayah');
?>

    <div class="form-group">
        <label>Kelurahan/Nama Wilayah</label>
        <input name="nama_wilayah" placeholder="Nama wilayah" class="form-control" required>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Latitude</label>
            <input name="latitude" id="Latitude" placeholder="Latitude" class="form-control" required readonly>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Longitude</label>
            <input name="longitude" id="Longitude" placeholder="Longitude" class="form-control" required readonly>
        </div>
    </div>

    <div class="form-group">
        <label>Provinsi</label>
        <input name="provinsi" placeholder="Nama provinsi" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Kabupaten/Kota</label>
        <input name="kabupaten" placeholder="Nama kabupaten" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Kecamatan</label>
        <input name="kecamatan" placeholder="Nama Kecamatan" class="form-control" required>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Simpan</button>
        <button type="reset" class="btn btn-primary">Reset</button>
    </div>

<?php
echo form_close();
?>
</div>

<script>

var curLocation=[0,0];
if (curLocation[0]==0 && curLocation[1]==0) {
	curLocation =[-6.178784935825187, 106.82556670943676];	
}

var map = L.map('map').setView([-6.342526, 106.866983], 10);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11'
}).addTo(map);

map.attributionControl.setPrefix(false);

var marker = new L.marker(curLocation, {
	draggable:'true'
});

marker.on('dragend', function(event) {
var position = marker.getLatLng();
marker.setLatLng(position,{
	draggable : 'true'
	}).bindPopup(position).update();
	$("#Latitude").val(position.lat);
	$("#Longitude").val(position.lng).keyup();
});

$("#Latitude, #Longitude").change(function(){
	var position =[parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
	marker.setLatLng(position, {
	draggable : 'true'
	}).bindPopup(position).update();
	map.panTo(position);
});
map.addLayer(marker);
</script>