<?php
if($this->session->flashdata('pesan')){
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('pesan');
    echo '</div>';
}else if($this->session->flashdata('gagal_delete')){
    echo '<div class="alert alert-danger">';
    echo $this->session->flashdata('gagal_delete');
    echo '</div>';
}

echo form_open('home/input_wilayah');
?>

<table class="table table-responsive table-striped table-bordered table-hover" id="table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama Wilayah</th>
            <th>Provinsi</th>
            <th>Kabupaten</th>
            <th>Kecamatan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($wilayah as $key => $value) { ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $value['nama_wilayah'] ?></td>
                <td><?= $value['provinsi'] ?></td>
                <td><?= $value['kabupaten'] ?></td>
                <td><?= $value['kecamatan'] ?></td>
                <td>
                    <a href="<?= base_url('home/edit_wilayah/'.$value['wilayah_id']) ?>" class="btn btn-xs btn-success">Edit</a>
                    <a href="<?= base_url('home/delete_wilayah/'.$value['wilayah_id']) ?>" class="btn btn-xs btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>