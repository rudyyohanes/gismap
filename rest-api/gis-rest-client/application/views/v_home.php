<div id="map" style="height:500px"></div>

<script>
    var map = L.map('map').setView([-6.342526, 106.866983], 10);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox/streets-v11'
    }).addTo(map);

    <?php foreach ($pemetaan as $key => $value) { ?>
        L.circle([<?= $value['latitude'] ?>, <?= $value['longitude'] ?>], {
            radius: <?= $value['radius'] ?>,
            color: '',
            fillColor: '<?= $value['warna'] ?>',
            fillOpacity: 0.5,
            }).bindPopup("<b><?= $value['nama_wilayah'] ?></b><br>Game Computing : <?= $value['jml_gc'] ?><br>Informatika : <?= $value['jml_if'] ?>").addTo(map);
    <?php } ?>

</script>